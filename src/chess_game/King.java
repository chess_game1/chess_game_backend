package chess_game;

public class King extends ChessPiece {
	
	private static int PIECE_VALUE = 99;
	private static MovingRange MOVING_RANGE = MovingRange.ONE_TWO;
	private static MovingDirection ALLOWED_MOVING_DIRECTIONS[] = {MovingDirection.KING};
	
	public King(ChessPieceOwner owner) {
			super(owner);
	}

	@Override
	public String toString() {
		return "K";
	}

	@Override
	public Boolean isValidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		Position distance = Position.calculateDistance(origin, target);

		if (distance.getPositionX() == 0 && distance.getPositionY() == 1) 
			return true;
		else if (distance.getPositionX() == 0 && distance.getPositionY() == -1) 
			return true;
		else if (distance.getPositionY() == 0 && distance.getPositionX() == 1) 
			return true;
		else if (distance.getPositionY() == 0 && distance.getPositionX() == -1) 
			return true;
		else if (distance.getPositionX() == distance.getPositionY() && distance.getPositionY() == 1) 
			return true;
		else if (distance.getPositionX() == distance.getPositionY() * -1 && distance.getPositionY() == -1) 
			return true;
		else if (distance.getPositionX() * -1 == distance.getPositionY() && distance.getPositionX() == 1) 
			return true;
		else if (distance.getPositionX() == distance.getPositionY() && distance.getPositionX() == -1) 
			return true;
		else return false;
	}

	@Override
	public Boolean noChessPieceInTheWay(ChessPiece[][] chessboard, Position origin, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isTargetPositionNotEmpty(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isPossibleToCapture(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPieceValue() {
		return PIECE_VALUE;
	}

	@Override
	public Boolean isInvalidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		return !isValidMove(chessboard, origin, target);
	}

}
