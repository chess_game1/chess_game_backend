package chess_game;

public class Queen extends ChessPiece {
	
	private static int PIECE_VALUE = 8;
	private static MovingRange MOVING_RANGE = MovingRange.ONE_TWO;
	private static MovingDirection ALLOWED_MOVING_DIRECTIONS[] = {MovingDirection.HORIZONTAL,MovingDirection.VERTICAL,MovingDirection.DIAGONAL};
	
	public Queen(ChessPieceOwner owner) {
			super(owner);
	}

	@Override
	public String toString() {
		return "Q";
	}

	@Override
	public Boolean isValidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		Position distance = Position.calculateDistance(origin, target);

		if (distance.getPositionX() == 0 && distance.getPositionY() >= 1 && distance.getPositionY() < Chessboard.Y) 
			return true;
		else if (distance.getPositionX() == 0 && distance.getPositionY() <= -1 && distance.getPositionX() > (Chessboard.Y * -1)) 
			return true;
		else if (distance.getPositionY() == 0 && distance.getPositionX() >= 1 && distance.getPositionX() < Chessboard.X) 
			return true;
		else if (distance.getPositionY() == 0 && distance.getPositionX() <= -1 && distance.getPositionX() > (Chessboard.X * -1))
			return true;
		else if (distance.getPositionX() == distance.getPositionY() && distance.getPositionY() >= 1 && distance.getPositionY() < Chessboard.Y) 
			return true;
		else if (distance.getPositionX() == distance.getPositionY() * -1 && distance.getPositionY() <= -1 && distance.getPositionX() > (Chessboard.Y * -1)) 
			return true;
		else if (distance.getPositionX() * -1 == distance.getPositionY() && distance.getPositionX() >= 1 && distance.getPositionX() < Chessboard.X) 
			return true;
		else if (distance.getPositionX() == distance.getPositionY() && distance.getPositionX() <= -1 && distance.getPositionX() > (Chessboard.X * -1)) 
			return true;
		else return false;
	}

	@Override
	public Boolean noChessPieceInTheWay(ChessPiece[][] chessboard, Position origin, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isTargetPositionNotEmpty(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isPossibleToCapture(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPieceValue() {
		return PIECE_VALUE;
	}

	@Override
	public Boolean isInvalidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		return !isValidMove(chessboard, origin, target);
	}

}
