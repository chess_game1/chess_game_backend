package chess_game;

public abstract class ChessPiece {
	
	public static int PIECE_VALUE;
	private ChessPieceOwner owner;
	private MovingDirection movingDirection;

	public ChessPiece(ChessPieceOwner owner, MovingDirection movingDirection) {
		this.owner = owner;
		this.movingDirection = movingDirection;
	}

	public ChessPiece(ChessPieceOwner owner) {
		this.owner = owner;
	}

	public ChessPieceOwner getOwner() {
		return owner;
	}

	public MovingDirection getMovingDirection() {
		return movingDirection;
	}
	
	public abstract int getPieceValue();	
	public abstract Boolean isValidMove (ChessPiece[][] chessboard, Position origin, Position target);
	public abstract Boolean isInvalidMove (ChessPiece[][] chessboard, Position origin, Position target);
	public abstract Boolean noChessPieceInTheWay (ChessPiece[][] chessboard, Position origin, Position target);
	public abstract  Boolean isTargetPositionNotEmpty (ChessPiece[][] chessboard_, Position target);
	public abstract  Boolean isPossibleToCapture (ChessPiece[][] chessboard_, Position target);
}
