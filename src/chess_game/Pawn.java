package chess_game;

public class Pawn extends ChessPiece {
	
	private static int PIECE_VALUE = 1;
	private static MovingRange MOVING_RANGE = MovingRange.ONE_TWO;
	private static MovingDirection ALLOWED_MOVING_DIRECTIONS[] = {MovingDirection.UP, MovingDirection.DOWN};
	
	public Pawn (ChessPieceOwner owner, MovingDirection movingDirection) {
			super(owner, movingDirection);
	}

	@Override
	public String toString () {
		return "P";
	}

	@Override
	public Boolean isValidMove (ChessPiece[][] chessboard, Position origin, Position target) {

		MovingDirection movingDirection = this.getMovingDirection();
	
		if (movingDirection == MovingDirection.DOWN) {
			return isValidMoveDown (chessboard, origin, target);
		} else if (movingDirection == MovingDirection.UP) {
			return isValidMoveUp (chessboard, origin, target);
		} else return false;
	}
	
	private Boolean isValidMoveDown (ChessPiece[][] chessboard, Position origin, Position target) {
		
		Position distance = Position.calculateDistance(origin, target);
		
		if (distance.getPositionX() == 0 && (distance.getPositionY() == 1 
				|| (origin.getPositionY() == 1 && distance.getPositionY() == 2))) 
			return noChessPieceInTheWay(chessboard, origin, target);
		else if ((distance.getPositionX() == 1 && distance.getPositionY() == 1
				|| distance.getPositionX() == -1 && distance.getPositionY() == 1)) 
			return isPossibleToCapture(chessboard, target);
		else return false;
	}
	
	private Boolean isValidMoveUp (ChessPiece[][] chessboard, Position origin, Position target) {
		
		Position distance = Position.calculateDistance(origin, target);
		
		if (distance.getPositionX() == 0 && (distance.getPositionY() == -1 
				|| (origin.getPositionY() == 6 && distance.getPositionY() == -2))) 
			return noChessPieceInTheWay(chessboard, origin, target);
		else if ((distance.getPositionX() == -1 && distance.getPositionY() == -1
				|| distance.getPositionX() == 1 && distance.getPositionY() == -1)) 
			return isPossibleToCapture(chessboard, target);
		else return false;
	}

	@Override
	public Boolean noChessPieceInTheWay (ChessPiece[][] chessboard, Position origin, Position target) {
		
		if (isTargetPositionNotEmpty(chessboard, target)) return false;
		
		if (this.getMovingDirection() == MovingDirection.DOWN) {
			for (int y = origin.getPositionY() + 1; y < target.getPositionY(); y++) {
				if  (chessboard[origin.getPositionX()][y] != null) {
					return false;
				}
			}
		} else if (this.getMovingDirection() == MovingDirection.UP) {
			for (int y = origin.getPositionY() - 1; y < target.getPositionY(); y--) {
				if  (chessboard[origin.getPositionX()][y] != null) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	@Override
	public  Boolean isTargetPositionNotEmpty (ChessPiece[][] chessboard_, Position target) {
		ChessPiece targetPiece = chessboard_[target.getPositionX()][target.getPositionY()];
		if (targetPiece != null) return true;
		else return false;
	}
	
	@Override
	public  Boolean isPossibleToCapture (ChessPiece[][] chessboard_, Position target) {
		ChessPiece targetPiece = chessboard_[target.getPositionX()][target.getPositionY()];
		if (targetPiece != null && targetPiece.getOwner() != this.getOwner()) return true;
		else return false;
	}

	@Override
	public int getPieceValue() {
		return PIECE_VALUE;
	}

	@Override
	public Boolean isInvalidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		return !isValidMove(chessboard, origin, target);
	}

}
