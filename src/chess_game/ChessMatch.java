package chess_game;

public class ChessMatch {
	
	
	public static void main(String args[]) {
		System.out.println("INITIALIZE GAME");
		
		Chessboard chessboard = initializeChessboard();
		
		System.out.println("START GAME");

		System.out.println("");
		System.out.println(chessboard);
		System.out.println("");
		
		chessboard.moveChessPiece(Position.H_TWO, Position.H_FOUR);
		System.out.println(chessboard);
		
		chessboard.moveChessPiece(Position.G_SEVEN, Position.G_FIVE);
		System.out.println(chessboard);
		
		chessboard.moveChessPiece(Position.H_FOUR, Position.G_FIVE);
		System.out.println(chessboard);
		
		System.out.println("END GAME");
		
	}
	
	static Chessboard initializeChessboard() {
		Chessboard chessboard = new Chessboard();

		chessboard.placeChessPiece(new Rook(ChessPieceOwner.WHITE), Position.H_ONE);
		chessboard.placeChessPiece(new Knight(ChessPieceOwner.WHITE), Position.G_ONE);
		chessboard.placeChessPiece(new Bishop(ChessPieceOwner.WHITE), Position.F_ONE);
		chessboard.placeChessPiece(new King(ChessPieceOwner.WHITE), Position.E_ONE);
		chessboard.placeChessPiece(new Queen(ChessPieceOwner.WHITE), Position.D_ONE);
		chessboard.placeChessPiece(new Bishop(ChessPieceOwner.WHITE), Position.C_ONE);
		chessboard.placeChessPiece(new Knight(ChessPieceOwner.WHITE), Position.B_ONE);
		chessboard.placeChessPiece(new Rook(ChessPieceOwner.WHITE), Position.A_ONE);
		
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.H_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.G_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.F_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.E_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.D_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.C_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.B_TWO);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN), Position.A_TWO);
		
		
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.H_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.G_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.F_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.E_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.D_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.C_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.B_SEVEN);
		chessboard.placeChessPiece(new Pawn(ChessPieceOwner.BLACK, MovingDirection.UP), Position.A_SEVEN);

		chessboard.placeChessPiece(new Rook(ChessPieceOwner.BLACK), Position.H_EIGHT);
		chessboard.placeChessPiece(new Knight(ChessPieceOwner.BLACK), Position.G_EIGHT);
		chessboard.placeChessPiece(new Bishop(ChessPieceOwner.BLACK), Position.F_EIGHT);
		chessboard.placeChessPiece(new King(ChessPieceOwner.BLACK), Position.E_EIGHT);
		chessboard.placeChessPiece(new Queen(ChessPieceOwner.BLACK), Position.D_EIGHT);
		chessboard.placeChessPiece(new Bishop(ChessPieceOwner.BLACK), Position.C_EIGHT);
		chessboard.placeChessPiece(new Knight(ChessPieceOwner.BLACK), Position.B_EIGHT);
		chessboard.placeChessPiece(new Rook(ChessPieceOwner.BLACK), Position.A_EIGHT);
		
		return chessboard;
	}
}
