package chess_game;

public enum MovingDirection {
	
	DOWN,
	UP,
	LEFT,
	RIGHT,
	DIAGONAL,
	HORSE,
	KING,
	QUEEN,
	VERTICAL,
	HORIZONTAL

}
