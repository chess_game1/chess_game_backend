package chess_game;

public class Knight extends ChessPiece {
	
	private static int PIECE_VALUE = 3;
	private static MovingRange MOVING_RANGE = MovingRange.ONE_TWO;
	private static MovingDirection ALLOWED_MOVING_DIRECTIONS[] = {MovingDirection.HORSE};
	
	public Knight(ChessPieceOwner owner) {
			super(owner);
	}

	@Override
	public String toString() {
		return "H";
	}

	@Override
	public Boolean isValidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		Position distance = Position.calculateDistance(origin, target);

		if (distance.getPositionX() == -1 && distance.getPositionY() == 2)
			return true;
		else if (distance.getPositionX() == 1 && distance.getPositionY() == 2) 
			return true;
		else if (distance.getPositionX() == -1 && distance.getPositionY() == -2)
			return true;
		else if (distance.getPositionX() == 1 && distance.getPositionY() == -2) 
			return true;
		else if (distance.getPositionX() == -2 && distance.getPositionY() == 1)
			return true;
		else if (distance.getPositionX() == 2 && distance.getPositionY() == 1) 
			return true;
		else if (distance.getPositionX() == -2 && distance.getPositionY() == -1)
			return true;
		else if (distance.getPositionX() == 2 && distance.getPositionY() == -1) 
			return true;
		else return false;
	}

	@Override
	public Boolean noChessPieceInTheWay(ChessPiece[][] chessboard, Position origin, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isTargetPositionNotEmpty(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isPossibleToCapture(ChessPiece[][] chessboard_, Position target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPieceValue() {
		return PIECE_VALUE;
	}

	@Override
	public Boolean isInvalidMove(ChessPiece[][] chessboard, Position origin, Position target) {
		return !isValidMove(chessboard, origin, target);
	}

}
