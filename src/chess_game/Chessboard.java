package chess_game;

public class Chessboard {
	static int X = 8;
	static int Y = 8;

	private ChessPiece chessboard[][] = new ChessPiece[X][Y];

	public ChessPiece[][] getChessboard() {
		return chessboard;
	}
	
	public Boolean placeChessPiece (ChessPiece chessPiece, Position origin) {
		// TODO: Plausibilitätschecks
		// TODO: Capture Piece Checks
		if (isPositionOutsideChessboard(origin)) return false;

		setChessPieceOnChessboard(chessPiece,origin);
		
		return true;
	}
	
	public Boolean moveChessPiece (Position origin, Position target) {
		// Capture Piece Checks
		ChessPiece chessPiece = getChessPieceFromChessboard(origin);
		
		if (chessPiece == null) return false;
		if (isPositionOutsideChessboard(target)) return false;
		if (chessPiece.isInvalidMove(chessboard, origin, target)) return false;

		Boolean isOriginPlaced = setChessPieceOnChessboard(null,origin);
		Boolean isTargetPlaced = setChessPieceOnChessboard(chessPiece,target);
		
		return isOriginPlaced && isTargetPlaced;
	}
	
	private Boolean setChessPieceOnChessboard (ChessPiece chessPiece, Position position) {
		chessboard[position.getPositionX()][position.getPositionY()] = chessPiece;
		return true;
	}
	
	private ChessPiece getChessPieceFromChessboard (Position position) {
		return chessboard[position.getPositionX()][position.getPositionY()];
	}
	
	@Override
	public String toString() {
		String chessboardPrint =  "";
		System.out.println("   H  G  F  E  D  C  B  A");
		System.out.println("");
		for (int y=0;y<Y;y++) {
			String row = (y+1) + "  ";
			for (int x=0;x<X;x++) {
				String chessPiece = " ";
				if (chessboard[x][y] != null) chessPiece = chessboard[x][y].toString();
				row += chessPiece + "  ";
			}
			//row += "/n";
			//chessboardPrint += row;
//			System.out.println("");
			System.out.println(row);
//			System.out.println("");
		}
		return chessboardPrint;
	}
	
	public static Boolean isPositionOnChessboard (Position position) {
		if (position.getPositionX() >= 0 && position.getPositionX() < X
				&& position.getPositionY() >= 0 && position.getPositionY() < Y) return true;
		else return false;
	}
	
	public static Boolean isPositionOutsideChessboard (Position position) {
		return !isPositionOnChessboard(position);
	}
	
}
