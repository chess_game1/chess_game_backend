package chess_game;

public class Position {
	
	// X
	static int A = 7;
	static int B = 6;
	static int C = 5;
	static int D = 4;
	static int E = 3;
	static int F = 2;
	static int G = 1;
	static int H = 0;

	static int ONE = 0;
	static int TWO = 1;
	static int THREE = 2;
	static int FOUR = 3;
	static int FIVE = 4;
	static int SIX = 5;
	static int SEVEN = 6;
	static int EIGHT = 7;

	// Y
	static Position H_ONE = new Position(H,ONE);
	static Position G_ONE = new Position(G,ONE);
	static Position F_ONE = new Position(F,ONE);
	static Position E_ONE = new Position(E,ONE);
	static Position D_ONE = new Position(D,ONE);
	static Position C_ONE = new Position(C,ONE);
	static Position B_ONE = new Position(B,ONE);
	static Position A_ONE = new Position(A,ONE);
	
	static Position H_TWO = new Position(H,TWO);
	static Position G_TWO = new Position(G,TWO);
	static Position F_TWO = new Position(F,TWO);
	static Position E_TWO = new Position(E,TWO);
	static Position D_TWO = new Position(D,TWO);
	static Position C_TWO = new Position(C,TWO);
	static Position B_TWO = new Position(B,TWO);
	static Position A_TWO = new Position(A,TWO);
	
	static Position H_THREE = new Position(H,THREE);
	static Position G_THREE = new Position(G,THREE);
	static Position F_THREE = new Position(F,THREE);
	static Position E_THREE = new Position(E,THREE);
	static Position D_THREE = new Position(D,THREE);
	static Position C_THREE = new Position(C,THREE);
	static Position B_THREE = new Position(B,THREE);
	static Position A_THREE = new Position(A,THREE);
	
	static Position H_FOUR = new Position(H,FOUR);
	static Position G_FOUR = new Position(G,FOUR);
	static Position F_FOUR = new Position(F,FOUR);
	static Position E_FOUR = new Position(E,FOUR);
	static Position D_FOUR = new Position(D,FOUR);
	static Position C_FOUR = new Position(C,FOUR);
	static Position B_FOUR = new Position(B,FOUR);
	static Position A_FOUR = new Position(A,FOUR);

	static Position H_FIVE = new Position(H,FIVE);
	static Position G_FIVE = new Position(G,FIVE);
	static Position F_FIVE = new Position(F,FIVE);
	static Position E_FIVE = new Position(E,FIVE);
	static Position D_FIVE = new Position(D,FIVE);
	static Position C_FIVE = new Position(C,FIVE);
	static Position B_FIVE = new Position(B,FIVE);
	static Position A_FIVE = new Position(A,FIVE);

	static Position H_SIX = new Position(H,SIX);
	static Position G_SIX = new Position(G,SIX);
	static Position F_SIX = new Position(F,SIX);
	static Position E_SIX = new Position(E,SIX);
	static Position D_SIX = new Position(D,SIX);
	static Position C_SIX = new Position(C,SIX);
	static Position B_SIX = new Position(B,SIX);
	static Position A_SIX = new Position(A,SIX);

	static Position H_SEVEN = new Position(H,SEVEN);
	static Position G_SEVEN = new Position(G,SEVEN);
	static Position F_SEVEN = new Position(F,SEVEN);
	static Position E_SEVEN = new Position(E,SEVEN);
	static Position D_SEVEN = new Position(D,SEVEN);
	static Position C_SEVEN = new Position(C,SEVEN);
	static Position B_SEVEN = new Position(B,SEVEN);
	static Position A_SEVEN = new Position(A,SEVEN);

	static Position H_EIGHT = new Position(H,EIGHT);
	static Position G_EIGHT = new Position(G,EIGHT);
	static Position F_EIGHT = new Position(F,EIGHT);
	static Position E_EIGHT = new Position(E,EIGHT);
	static Position D_EIGHT = new Position(D,EIGHT);
	static Position C_EIGHT = new Position(C,EIGHT);
	static Position B_EIGHT = new Position(B,EIGHT);
	static Position A_EIGHT = new Position(A,EIGHT);
	
	private int x = 0;
	private int y = 0;
	
	public Position (int x, int y) {
		if (this.validPositionX(x)) {
			this.x= x;
		}		
		if (this.validPositionY(y)) {
			this.y= y;
		} 
	}
	
	Boolean setPositionX(int x) {
		if (this.validPositionX(x)) {
			this.x= x;
			return true;
		} else return false;
		
	}
	
	Boolean setPositionY(int y) {
		if (this.validPositionY(y)) {
			this.y= y;
			return true;
		} else return false;
	}
	
	void setPositionXwithoutValidation(int x) {
		this.x= x;	
	}
	
	public void setPositionYwithoutValidation(int y) {
		this.y= y;
	}
	
	int getPositionX() {
		return this.x;
	}
	
	int getPositionY() {
		return this.y;
	}
	
	static Position calculateDistance (Position origin, Position target) {
		Position distance = new Position(0,0);
		
		distance.setPositionXwithoutValidation(target.getPositionX() - origin.getPositionX());
		distance.setPositionYwithoutValidation(target.getPositionY() - origin.getPositionY());
		
		return distance;
	}
	
	private Boolean validPositionX(int x) {
		if (x >= 0 && x < Chessboard.X) return true;
		else return false;
	}
	
	private Boolean validPositionY(int y) {
		if (y >= 0 && y < Chessboard.Y) return true;
		else return false;
	}
}
