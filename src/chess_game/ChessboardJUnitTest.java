package chess_game;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ChessboardJUnitTest {
	
	private static Boolean DEBUG = true;
	
	@Test
	void testValidPositionPositiv() {

		Position origin = new Position(9,9);
		
		assertTrue(origin.getPositionX() == 0 && origin.getPositionY() == 0);
	}
	
	@Test
	void testValidPositionNegativ() {

		Position origin = new Position(-3,-3);
		
		assertTrue(origin.getPositionX() == 0 && origin.getPositionY() == 0);
	}
	
	/**
	@Test
	void testNewPawn() {
		
		ChessPiece chessPiece = new Pawn(ChessPieceOwner.WHITE, MovingDirection.DIAGONAL);
		
		// TODO: Setzen einer nicht nicht validen MovingDirection ist noch möglich!
		assertTrue(!(chessPiece.getMovingDirection() == MovingDirection.DIAGONAL));
	}
	*/
	
	@Test
	void testChessMatchInitialize() {

		Chessboard chessboard = ChessMatch.initializeChessboard();
		ChessPiece chessPiece[][] = chessboard.getChessboard();
		
		Boolean assertion = true;

		for (int y=0; y < Chessboard.Y; y++) {
			for (int x=0; x < Chessboard.X; x++) {
				if (y == Position.TWO || y == Position.SEVEN) {
					if (!(chessPiece[x][y] instanceof Pawn)) assertion = false;
				} else if (y == Position.ONE || y == Position.EIGHT) {
					if (x == Position.H || x == Position.A) {
						if (!(chessPiece[x][y] instanceof Rook)) assertion = false;
					} else if (x == Position.G || x == Position.B) {
						if (!(chessPiece[x][y] instanceof Knight)) assertion = false;
					} else if (x == Position.F || x == Position.C) {
						if (!(chessPiece[x][y] instanceof Bishop)) assertion = false;
					} else if (x == Position.E) {
						if (!(chessPiece[x][y] instanceof King)) assertion = false;
					} else if (x == Position.D) {
						if (!(chessPiece[x][y] instanceof Queen)) assertion = false;
					}
				} else if (chessPiece[x][y] != null) assertion = false;
			}
		}
		
		assertTrue(assertion);
	}
	
	@Test
	void testPlaceChessPiece() {

		Chessboard chessboard = new Chessboard();
		
		Position origin = new Position(Position.A,Position.ONE);
		
		ChessPiece chessPiece = new Pawn(ChessPieceOwner.WHITE, MovingDirection.HORSE);
		
		assertTrue(chessboard.placeChessPiece(chessPiece, origin));
	}

	@Test
	void testPawnMoveOneStepDown() {

		Chessboard chessboard = new Chessboard();
		
		Position origin = new Position(Position.H,Position.TWO);

		ChessPiece chessPiece = new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN);
		
		chessboard.placeChessPiece(chessPiece, origin);
		
		chessboard.moveChessPiece(Position.H_TWO, Position.H_THREE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		ChessPiece pawn_H_THREE = chessboard.getChessboard()[Position.H][Position.THREE];
		
		assertTrue(pawn_H_TWO == null && pawn_H_THREE instanceof Pawn);
	}

	@Test
	void testRookMoveOneStepDown() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece rook = new Rook(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(rook, Position.H_ONE);
		
		chessboard.moveChessPiece(Position.H_ONE, Position.H_TWO);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_ONE = chessboard.getChessboard()[Position.H][Position.ONE];
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		
		assertTrue(pawn_H_ONE == null && pawn_H_TWO instanceof Rook);
	}

	@Test
	void testRookMoveThreeStepDown() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece rook = new Rook(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(rook, Position.H_ONE);
		
		chessboard.moveChessPiece(Position.H_ONE, Position.H_FOUR);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_ONE = chessboard.getChessboard()[Position.H][Position.ONE];
		ChessPiece pawn_H_FOUR = chessboard.getChessboard()[Position.H][Position.FOUR];
		
		assertTrue(pawn_H_ONE == null && pawn_H_FOUR instanceof Rook);
	}

	@Test
	void testRookMoveThreeStepsRight() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece rook = new Rook(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(rook, Position.H_ONE);
		
		chessboard.moveChessPiece(Position.H_ONE, Position.D_ONE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_ONE = chessboard.getChessboard()[Position.H][Position.ONE];
		ChessPiece pawn_D_ONE = chessboard.getChessboard()[Position.D][Position.ONE];
		
		assertTrue(pawn_H_ONE == null && pawn_D_ONE instanceof Rook);
	}

	@Test
	void testRookMoveThreeStepsLeft() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece rook = new Rook(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(rook, Position.D_ONE);
		
		chessboard.moveChessPiece(Position.D_ONE, Position.H_ONE);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_ONE = chessboard.getChessboard()[Position.D][Position.ONE];
		ChessPiece pawn_H_ONE = chessboard.getChessboard()[Position.H][Position.ONE];
		
		assertTrue(pawn_D_ONE == null && pawn_H_ONE instanceof Rook);
	}

	@Test
	void testRookMoveThreeStepsUp() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece rook = new Rook(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(rook, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.D_ONE);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_D_ONE = chessboard.getChessboard()[Position.D][Position.ONE];
		
		assertTrue(pawn_D_FOUR == null && pawn_D_ONE instanceof Rook);
	}

	@Test
	void testKnightMoveDownRight() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece knight = new Knight(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(knight, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.C_SIX);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_C_SIX = chessboard.getChessboard()[Position.C][Position.SIX];
		
		assertTrue(pawn_D_FOUR == null && pawn_C_SIX instanceof Knight);
	}

	@Test
	void testKnightMoveUpLeft() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece knight = new Knight(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(knight, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.E_TWO);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_E_TWO = chessboard.getChessboard()[Position.E][Position.TWO];
		
		assertTrue(pawn_D_FOUR == null && pawn_E_TWO instanceof Knight);
	}

	@Test
	void testKingMoveUpLeft() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece king = new King(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(king, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.C_THREE);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_C_THREE = chessboard.getChessboard()[Position.C][Position.THREE];
		
		assertTrue(pawn_D_FOUR == null && pawn_C_THREE instanceof King);
	}

	@Test
	void testQueenMoveUpLeft() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece queen = new Queen(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(queen, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.B_TWO);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_B_TWO = chessboard.getChessboard()[Position.B][Position.TWO];
		
		assertTrue(pawn_D_FOUR == null && pawn_B_TWO instanceof Queen);
	}

	@Test
	void testBishoptMoveDownRight() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece bishop = new Bishop(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(bishop, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.B_SIX);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_B_SIX = chessboard.getChessboard()[Position.B][Position.SIX];
		
		assertTrue(pawn_D_FOUR == null && pawn_B_SIX instanceof Bishop);
	}

	@Test
	void testBishoptMoveUpLeft() {
	
		Chessboard chessboard = new Chessboard();
		
		ChessPiece bishop = new Bishop(ChessPieceOwner.WHITE);
		
		chessboard.placeChessPiece(bishop, Position.D_FOUR);
		
		chessboard.moveChessPiece(Position.D_FOUR, Position.F_TWO);
		if (DEBUG) System.out.println(chessboard);

		ChessPiece pawn_D_FOUR = chessboard.getChessboard()[Position.D][Position.FOUR];
		ChessPiece pawn_F_TWO = chessboard.getChessboard()[Position.F][Position.TWO];
		
		assertTrue(pawn_D_FOUR == null && pawn_F_TWO instanceof Bishop);
	}

	@Test
	void testPawnMoveOneStepUp() {
	
		Chessboard chessboard = ChessMatch.initializeChessboard();
		
		chessboard.moveChessPiece(Position.H_SEVEN, Position.H_SIX);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_SEVEN = chessboard.getChessboard()[Position.H][Position.SEVEN];
		ChessPiece pawn_H_SIX = chessboard.getChessboard()[Position.H][Position.SIX];
		
		assertTrue(pawn_H_SEVEN == null && pawn_H_SIX instanceof Pawn);
	}

	@Test
	void testPawnMoveTwoStepsDown() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();
		
		chessboard.moveChessPiece(Position.H_TWO, Position.H_FOUR);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		ChessPiece pawn_H_FOUR = chessboard.getChessboard()[Position.H][Position.FOUR];
		
		assertTrue(pawn_H_TWO == null && pawn_H_FOUR instanceof Pawn);
	}

	@Test
	void testPawnMoveThreeStepsDown() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();
		
		chessboard.moveChessPiece(Position.H_TWO, Position.H_FIVE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		ChessPiece pawn_H_FIVE = chessboard.getChessboard()[Position.H][Position.FIVE];
		
		assertTrue(pawn_H_FIVE == null && pawn_H_TWO instanceof Pawn);
	}

	@Test
	void testPawnMovesOffBoard() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();
		Position offBoardPosition = new Position(-1,Position.THREE);
		
		offBoardPosition.setPositionXwithoutValidation(-1);
		offBoardPosition.setPositionYwithoutValidation(Position.THREE); 
		
		chessboard.moveChessPiece(Position.H_TWO, offBoardPosition);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		
		assertTrue(pawn_H_TWO instanceof Pawn);
	}

	@Test
	void testPawnCaptureLeftDown() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();

		chessboard.moveChessPiece(Position.H_TWO, Position.H_FOUR);
		chessboard.moveChessPiece(Position.G_SEVEN, Position.G_FIVE);
		chessboard.moveChessPiece(Position.H_FOUR, Position.G_FIVE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_WHITE_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		ChessPiece pawn_WHITE_H_FOUR = chessboard.getChessboard()[Position.H][Position.FOUR];
		ChessPiece pawn_WHITE_G_FIVE = chessboard.getChessboard()[Position.G][Position.FIVE];

		ChessPiece pawn_BLACK_G_SEVEN = chessboard.getChessboard()[Position.G][Position.SEVEN];
		
		assertTrue(pawn_WHITE_H_TWO == null && pawn_WHITE_H_FOUR == null 
				&& pawn_WHITE_G_FIVE instanceof Pawn && pawn_WHITE_G_FIVE.getOwner() == ChessPieceOwner.WHITE
				&& pawn_BLACK_G_SEVEN == null);
	}

	@Test
	void testPawnCaptureRightDown() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();

		chessboard.moveChessPiece(Position.G_TWO, Position.G_FOUR);
		if (DEBUG) System.out.println(chessboard);
		chessboard.moveChessPiece(Position.H_SEVEN, Position.H_FIVE);
		if (DEBUG) System.out.println(chessboard);
		chessboard.moveChessPiece(Position.G_FOUR, Position.H_FIVE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_WHITE_G_TWO = chessboard.getChessboard()[Position.G][Position.TWO];
		ChessPiece pawn_WHITE_G_FOUR = chessboard.getChessboard()[Position.G][Position.FOUR];
		ChessPiece pawn_WHITE_H_FIVE = chessboard.getChessboard()[Position.H][Position.FIVE];

		ChessPiece pawn_BLACK_H_SEVEN = chessboard.getChessboard()[Position.H][Position.SEVEN];
		
		if (DEBUG) System.out.println(pawn_WHITE_H_FIVE.getOwner() == ChessPieceOwner.WHITE);
		
		assertTrue(pawn_WHITE_G_TWO == null && pawn_WHITE_G_FOUR == null 
				&& pawn_WHITE_H_FIVE instanceof Pawn 
				&& pawn_WHITE_H_FIVE.getOwner() == ChessPieceOwner.WHITE
				&& pawn_BLACK_H_SEVEN == null);
	}

	@Test
	void testPawnCaptureRightDownOwnPiece() {

		Chessboard chessboard = new Chessboard();

		ChessPiece chessPiece_WHITE_H_TWO = new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN);
		ChessPiece chessPiece_WHITE_G_THREE = new Pawn(ChessPieceOwner.WHITE, MovingDirection.DOWN);
		
		chessboard.placeChessPiece(chessPiece_WHITE_H_TWO, Position.H_TWO);
		chessboard.placeChessPiece(chessPiece_WHITE_G_THREE, Position.G_THREE);
		if (DEBUG) System.out.println(chessboard);
		
		chessboard.moveChessPiece(Position.H_TWO, Position.G_THREE);
		if (DEBUG) System.out.println(chessboard);
		
		chessPiece_WHITE_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		chessPiece_WHITE_G_THREE = chessboard.getChessboard()[Position.G][Position.THREE];
		
		assertTrue(chessPiece_WHITE_H_TWO instanceof Pawn 
				&& chessPiece_WHITE_G_THREE instanceof Pawn);
	}

	@Test
	void testPawnCaptureLeftUp() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();
		if (DEBUG) System.out.println(chessboard);

		chessboard.moveChessPiece(Position.G_SEVEN, Position.G_FIVE);
		if (DEBUG) System.out.println(chessboard);
		chessboard.moveChessPiece(Position.H_TWO, Position.H_FOUR);
		if (DEBUG) System.out.println(chessboard);
		chessboard.moveChessPiece(Position.G_FIVE, Position.H_FOUR);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_WHITE_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];

		ChessPiece pawn_BLACK_G_SEVEN = chessboard.getChessboard()[Position.G][Position.SEVEN];
		ChessPiece pawn_BLACK_H_FOUR = chessboard.getChessboard()[Position.H][Position.FOUR];
		
		assertTrue(pawn_WHITE_H_TWO == null
				&& pawn_BLACK_G_SEVEN == null
				&& pawn_BLACK_H_FOUR instanceof Pawn 
				&& pawn_BLACK_H_FOUR.getOwner() == ChessPieceOwner.BLACK);
	}

	@Test
	void testPawnCaptureRightUp() {
		
		Chessboard chessboard = ChessMatch.initializeChessboard();

		chessboard.moveChessPiece(Position.H_SEVEN, Position.H_FIVE);
		chessboard.moveChessPiece(Position.G_TWO, Position.G_FOUR);
		chessboard.moveChessPiece(Position.H_FIVE, Position.G_FOUR);
		
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_WHITE_G_TWO = chessboard.getChessboard()[Position.G][Position.TWO];
		ChessPiece pawn_BLACK_G_FOUR = chessboard.getChessboard()[Position.G][Position.FOUR];
		ChessPiece pawn_BLACK_H_SEVEN = chessboard.getChessboard()[Position.H][Position.SEVEN];
		
		assertTrue(pawn_WHITE_G_TWO == null
				&& pawn_BLACK_G_FOUR instanceof Pawn 
				&& pawn_BLACK_G_FOUR.getOwner() == ChessPieceOwner.BLACK
				&& pawn_BLACK_H_SEVEN == null);
	}

	@Test
	void testPawnCaptureDownWithoutTarget() {
	
		Chessboard chessboard = ChessMatch.initializeChessboard();
		
		chessboard.moveChessPiece(Position.H_TWO, Position.G_THREE);
		if (DEBUG) System.out.println(chessboard);
		
		ChessPiece pawn_H_TWO = chessboard.getChessboard()[Position.H][Position.TWO];
		ChessPiece pawn_G_THREE = chessboard.getChessboard()[Position.G][Position.THREE];
		
		assertTrue(pawn_G_THREE == null && pawn_H_TWO instanceof Pawn);
	}
}
